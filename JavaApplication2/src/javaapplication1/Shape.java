package javaapplication1;

public abstract class Shape {
    private String name;
    
    public Shape(String name){
        this.name = name;
    }
    
    public abstract double getArea();
    public abstract double getPerimeter();

    public String toString(){
        return "Shape name: " + name + "\n"
                + "Area: " + getArea() + "\n"
                + "Perimeter: " + getPerimeter() + "\n";
    }    
}
