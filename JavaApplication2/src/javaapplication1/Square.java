package javaapplication1;

public class Square extends Shape{
    private double width;
    
    public Square(String name, double width){
        super(name);
        this.width = width;
    }
    
    public double getArea(){
        return width*width;
    }
        
    public double getPerimeter(){
        return 4*width;
    }
}
