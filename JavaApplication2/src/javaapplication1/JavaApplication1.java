package javaapplication1;

import java.util.ArrayList;

public class JavaApplication1 {
    
    public static void main(String[] args) {
        ArrayList<Shape> shapes = new ArrayList<>();
        shapes.add(new Circle("Small Circle", 5));
        shapes.add(new Square("Medium Square", 20));
        shapes.add(new Circle("Big Circle", 50));
        
        for(Shape shape : shapes)
            System.out.println(shape);            
    }    
}
