package gradingsystem;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AlertBox {
    
    public static void display(String title, String message){
        Stage resultBox = new Stage();
        
        resultBox.initModality(Modality.APPLICATION_MODAL);
                
        resultBox.setTitle("Results");
               
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));
        
        Text scenetitle = new Text(title);
        scenetitle.setFont (Font.font("Arial", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0, 2, 1);
        
        Label resultMsg = new Label(message);
        resultMsg.setTextFill(Color.FIREBRICK);
        grid.add(resultMsg, 1, 1);
        
        Button btnClose = new Button("OK");
        btnClose.setOnAction(e -> resultBox.close());
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn.getChildren().add(btnClose);
        grid.add(hbBtn, 1, 3);
        
        Scene scene = new Scene(grid, 300, 400);
        resultBox.setScene(scene);
        resultBox.showAndWait();
    }
}